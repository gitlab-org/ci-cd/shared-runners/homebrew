class Sshpass < Formula
  url 'https://downloads.sourceforge.net/project/sshpass/sshpass/1.09/sshpass-1.09.tar.gz'
  homepage 'https://sourceforge.net/projects/sshpass/'
  sha256 '71746e5e057ffe9b00b44ac40453bf47091930cba96bbea8dc48717dedc49fb7'
  head do
    url 'https://svn.code.sf.net/p/sshpass/code/trunk'

    depends_on 'autoconf' => :build
    depends_on 'automake' => :build
  end

  def install
    system "./bootstrap" if build.head?
    system "./configure", "--disable-debug",
                          "--disable-dependency-tracking",
                          "--disable-silent-rules",
                          "--prefix=#{prefix}"
    system "make", "install"
  end

  test do
    system "#{bin}/sshpass"
  end
end
